<p align="center"> <img src="https://media0.giphy.com/media/v1.Y2lkPTc5MGI3NjExaWhzN284dGZ5aXEzNWU3NGhxYWhvMW9uMjdpZ241cHd0cnBkaXhxYiZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9dHM/CQ3Is7eRAbIQmXGSVq/giphy.gif" alt="hello" width="400" height="300"/> </p>

Hi there! ✨ I'm Emily and I'm a Software Engineer from the San Francisco Bay Area 🌉

Find me on GitHub 🔎
https://github.com/emilyvictany

<a href="https://www.linkedin.com/in/emilyyim/" target="_blank">
  <img src="https://img.shields.io/badge/LinkedIn-%230077B5.svg?&style=flat-square&logo=linkedin&logoColor=white" alt="LinkedIn" height="25" width="90">
</a>

<br>

<br>
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/>

<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/>

<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/>

<img src="https://cdn.worldvectorlogo.com/logos/django.svg" alt="django" width="40" height="40"/>

---

**A bit about my background**: 💬

I was born and raised here in the Bay Area 🌞 I attended undergrad in Utah, where I studied marketing. After moving back to the Bay Area, my path pivoted to healthcare 💊 I've been in healthcare for the past 5+ years, where I most recently worked in inpatient pharmacy at a level one pediatric trauma center.

My work in healthcare really amplified my passion for helping others and connecting with people. It also showed me ways of how technology can improve patient care, and how we can always strive to make things better.

I pivoted to software engineering so that I can amplify my reach, continuing on a mission to make meaningful impacts on people's lives.


---
**Thanks for dropping by!** 🎉

<p align="center"> <img src="https://cdn-images-1.medium.com/v2/resize:fit:720/format:gif/0*41inHKnPhGb04HsO.gif" alt="pusheen"/> </p>
